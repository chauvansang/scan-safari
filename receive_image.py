from PIL import Image
import datetime
from imutils import paths
import imutils
import time
import os
from params import Params
from shutil import copyfile
from glyph import get_mask
import cv2
import numpy as np
from cut_image import cut_image
from mask_code import get_size_mask

params = Params()


def rgb_to_hsv(r, g, b):
    maxc = max(r, g, b)
    minc = min(r, g, b)
    v = maxc
    if minc == maxc:
        return 0.0, 0.0, v
    s = (maxc - minc) / maxc
    rc = (maxc - r) / (maxc - minc)
    gc = (maxc - g) / (maxc - minc)
    bc = (maxc - b) / (maxc - minc)
    if r == maxc:
        h = bc - gc
    elif g == maxc:
        h = 2.0 + rc - bc
    else:
        h = 4.0 + gc - rc
    h = (h / 6.0) % 1.0
    return h, s, v


GREEN_RANGE_MIN_HSV = (245, 0, 245)
GREEN_RANGE_MAX_HSV = (245, 0, 245)


def resize_image(image_array, name):
    size = get_size_mask(name)
    image_array = cv2.resize(image_array, (int(size[0]), int(size[1])))

    b_channel, g_channel, r_channel = cv2.split(image_array)

    alpha_channel = np.ones(b_channel.shape, dtype=b_channel.dtype) * 255  # creating a dummy alpha channel image.

    img_BGRA = cv2.merge((b_channel, g_channel, r_channel, alpha_channel))

    return img_BGRA


def flip_image(img_path):
    # load the image with imread()
    img = cv2.imread(img_path,cv2.IMREAD_UNCHANGED)
    img = cv2.flip(img, 1)
    cv2.imwrite(img_path, img)


def main(img_path):
    try:
        # filename = os.path.basename(img_path)
        # split_filename = filename.split('_')
        # if (len(split_filename)) > 0:
        #     filename = split_filename[0]

        """change format from PIL to opencv """
        pil_image = Image.open(img_path).convert('RGB')
        open_cv_image = np.array(pil_image)
        # Convert RGB to BGR
        open_cv_image = open_cv_image[:, :, ::-1].copy()
        background = img_path
        foreground = get_mask(open_cv_image)
        # im_bg = Image.open(background)

        """get mask"""
        im_foreground = Image.open('masks/' + foreground + '.png')
        """Cut image"""
        im_bg = cut_image(open_cv_image)
        im_bg = resize_image(im_bg, foreground)
        cv2.imwrite("test.png", im_bg)
        im_bg = cv2.cvtColor(im_bg, cv2.COLOR_BGRA2RGBA)

        im_pil = Image.fromarray(im_bg)

        # For reversing the operation:
        # im_bg = np.asarray(im_pil)

        im_bg = im_pil.convert('RGBA')

        im_bg = Image.alpha_composite(im_foreground, im_bg)

        if os.path.exists('masks/' + foreground + '.png'):
            # Load image and convert it to RGBA, so it contains alpha channel
            # im_bg = Image.open(background)
            # Go through all pixels and turn each 'green' pixel to transparent
            pix = im_bg.load()
            pix_foreground = im_foreground.load()
            width, height = im_bg.size
            for x in range(width):
                for y in range(height):
                    r2, g2, b2, a2 = pix_foreground[x, y]
                    min_h, min_s, min_v = GREEN_RANGE_MIN_HSV
                    max_h, max_s, max_v = GREEN_RANGE_MAX_HSV
                    if min_h <= r2 <= max_h and min_s <= g2 <= max_s and min_v <= b2 <= max_v:
                        pix[x, y] = (0, 0, 0, 0)

            now = datetime.datetime.now()
            time_t = now.strftime("%Y%m%d%H%M%S%f")
            file_saved = './output/' + time_t + '.png'
            file_copied = params.data_folder + foreground + '_' + time_t + '.png'
            # resized = im_bg.resize((1024, 768), Image.ANTIALIAS)
            im_bg.save(file_saved, 'png')
            # flip_image(file_saved)
            print(file_saved)
            if(foreground == "elephant"):
                flip_image(file_saved)
            if os.path.exists(params.data_folder):
                copyfile(file_saved, file_copied)
            os.remove(file_saved)
            os.remove(img_path)
    except Exception as e:
        print(e)


if __name__ == '__main__':
    while True:
        for imagePath in paths.list_images("./images"):
            main(imagePath)
        time.sleep(1)
