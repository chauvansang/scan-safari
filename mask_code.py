def get_mask_code(name):
    return {
        '1, 1, 0, 0, 1, 1, 1, 0, 0': "bunny",
        '1, 1, 0, 0, 1, 0, 1, 1, 0': "lion",
        '1, 1, 0, 0, 0, 1, 1, 0, 1': "lionface",
        '0, 0, 1, 1, 1, 0, 1, 0, 1': "monkey",
        '0, 1, 1, 0, 1, 0, 1, 0, 1': "butterfly",
        '1, 0, 1, 1, 1, 0, 0, 0, 1': "hippo",
        '1, 0, 1, 1, 1, 1, 0, 0, 0': "dinosaur1",
        '0, 1, 1, 0, 1, 0, 1, 1, 0': "dinosaur3",
        '1, 0, 0, 1, 1, 0, 1, 1, 0': "dinosaur2",
        '1, 1, 0, 0, 1, 0, 1, 0, 1': "giraffe",
        # '0, 1, 0, 0, 1, 1, 1, 0, 1': "cat",
        '0, 1, 0, 0, 1, 1, 1, 0, 1': "cow",
        '1, 0, 0, 1, 1, 1, 0, 1, 0': "horse",
        '0, 0, 0, 1, 1, 0, 1, 1, 1': "unicorn",
        '1, 0, 1, 1, 1, 0, 1, 0, 0': "elephant",
    }[name]


def get_size_mask(name):
    return {
        'bunny': [2017, 1412],
        'unicorn': [2017, 1412],
        'elephant': [2017, 1412],
        'horse': [2017, 1412],
        'cat': [2017, 1412],
        'giraffe': [2016, 1412],
        'dinosaur1': [2017, 1412],
        'dinosaur2': [2017, 1412],
        'dinosaur3': [2017, 1412],
        'hippo': [2017, 1412],
        'lion': [2017, 1412],
        'lionface': [2017, 1412],
        'monkey': [2017, 1412],
        'butterfly': [2017, 1412],

    }[name]
