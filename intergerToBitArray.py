import numpy as np
from  mask_code import get_mask_code
def bin_array(num, m):
    """Convert a positive integer num into an m-bit bit vector"""
    return np.array(list(np.binary_repr(num).zfill(m))).astype(np.int8)
def fromIntergerToBitArray(number, numberBit):
    d = np.array([number])
    result = (((d[:, None] & (1 << np.arange(numberBit)))) > 0).astype(int)
    return result[0]


def shifting(bitlist):
    out = 0
    for bit in bitlist:
        out = (out << 1) | bit
    return out
temp = '1, 0, 1, 0, 1, 0, 1, 0, 1'
list1 = [1, 0, 1, 0, 1, 0, 1, 0, 1]
str1 = ' ,'.join(str(list1))
print(get_mask_code('1, 1, 1, 1, 1, 1, 1, 1, 1'))
# t = str1 == '1 ,2 ,3'
# te =', '.join(str(x) for x in list1)
#
# print(get_mask_code(te))
# print(te)
# print (', '.join(str(x) for x in list1))
# print(str1)
