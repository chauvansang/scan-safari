import cv2
import numpy as np


filename = 'kid.jpg'

oriimg = cv2.imread(filename)

oriimg = cv2.resize(oriimg,(int(2017),int(1412)))

b_channel, g_channel, r_channel = cv2.split(oriimg)

alpha_channel = np.ones(b_channel.shape, dtype=b_channel.dtype) * 255 #creating a dummy alpha channel image.

img_BGRA = cv2.merge((b_channel, g_channel, r_channel, alpha_channel))

cv2.imwrite("kid.png",img_BGRA)