import cv2
from glyphfunctions import *
import json
from io import StringIO
from cut_image import cut_image
import  numpy as np
from mask_code import get_mask_code

QUADRILATERAL_POINTS = 4
SHAPE_RESIZE = 100.0
BLACK_THRESHOLD = 120
WHITE_THRESHOLD = 155


def get_mask(image):
    """Read code from image """
    mask_name = ''
    pts1 = np.float32([[0,0],[400,0],[0,400],[400,400]])
    pts2 = np.float32([[0,0],[400,0],[0,400],[400,400]])
    M = cv2.getPerspectiveTransform(pts1,pts2)
    dst = cv2.warpPerspective(image,M,(300,300))

    gray = cv2.cvtColor(dst, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (5, 5), 0)
    edges = cv2.Canny(gray, 100, 200)
    _,contours, _ = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv2.contourArea, reverse=True)[:10]

    """Check contour has four edge"""
    for contour in contours:
        perimeter = cv2.arcLength(contour, True)
        approx = cv2.approxPolyDP(contour, 0.01 * perimeter, True)

        if len(approx) == QUADRILATERAL_POINTS:
            topdown_quad = get_topdown_quad(gray, approx.reshape(4, 2))
            # cv2.imwrite("topdown_quad1.png",topdown_quad);
            resized_shape = resize_image(topdown_quad, SHAPE_RESIZE)
            if resized_shape[5, 5] > BLACK_THRESHOLD: continue
            glyph_found = False

            """get pattern in image"""
            glyph_pattern = get_glyph_pattern(resized_shape, BLACK_THRESHOLD, WHITE_THRESHOLD)
            temp =', '.join(str(x) for x in glyph_pattern)
            mask_name = get_mask_code(temp)

            """get mask image from name"""
            return  mask_name
